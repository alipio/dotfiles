alias ls="eza"
alias ll="eza --icons --git -la"
alias tree="eza --icons --tree"

alias cat="bat"
alias grep="grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}"
alias egrep="egrep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}"

# Git aliases
alias gco='git checkout'
alias gcb='git checkout -b'

alias gd='git diff'
alias gp='git push'
alias gpf='git push --force-with-lease'
alias gpl='git pull'

alias grbi='git rebase -i'
alias gci='git commit -m'
alias gcia='git commit -am'
alias gst='git status'
alias gad='git add'
alias gaa='git add .'

function gf
  git-foresta --style=10 --graph-margin-right=2 $argv | less -RSX
end

alias ber='bundle exec rspec'
alias be='bundle exec'

alias open='xdg-open'
alias fzb='fzf-books'

set fish_greeting

zoxide init fish | source
starship init fish | source

if test -f ~/.asdf/asdf.fish
  source ~/.asdf/asdf.fish
end

# Set up fzf key bindings
fzf --fish | source

fish_config theme choose "TokyoNight Storm"
