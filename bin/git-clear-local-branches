#!/bin/bash

set -u

_git_squashed_branches() {
  target_branch=$1
  git for-each-ref refs/heads/ --format="%(refname:short)" | while read -r branch; do
    merge_base=$(git merge-base "$target_branch" "$branch")
    commit="$(git commit-tree "$(git rev-parse "$branch^{tree}")" -p "$merge_base" -m _)"
    if [[ $(git cherry "$target_branch" "$commit") == "-"* ]]; then
      echo "$branch"
    fi
  done
}

default_branch=$(git remote show origin | sed -n '/HEAD branch/s/.*: //p')

git checkout "$default_branch" &>/dev/null
git pull --rebase origin "$default_branch" &>/dev/null

branches=$(git branch --merged | grep -v "^\*")

if [ "$branches" ]; then
  echo "Deleting merged branches..."
  echo "$branches" | xargs git branch -d
fi

branches=$(_git_squashed_branches "$default_branch")

if [ "$branches" ]; then
  echo "Deleting branches squashed into '$default_branch'..."
  echo "$branches" | xargs git branch -D
fi
