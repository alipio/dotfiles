#!/bin/bash

set -e
unalias -a

DOTFILES_ROOT=$(cd "${BASH_SOURCE%/*}/.." && pwd -P)
BIN_DIR="$DOTFILES_ROOT"/bin
export PATH="$BIN_DIR":$PATH

TEMP_DIR="$(mktemp -d)"

trap 'rm -rf "$TEMP_DIR"' 0

echo "Setting up local environment... This might take a while, be patient!"
sleep 2

echo -e "\nUpdating Arch"
sudo pacman -Syyu --noconfirm

echo -e "\nInstalling AUR dependencies"
# Everything required to build from the AUR (if you didn't install base-devel)
sudo pacman -S --noconfirm --needed base-devel git vim
# Build all packages (opening the PKGBUILD for each one)
aur light
aur rover

echo -e "\nSetting up GTK theme"
git clone https://github.com/Fausto-Korpsvart/Tokyonight-GTK-Theme.git --depth 1 "$TEMP_DIR"/tokyonight-gtk

mkdir -p ~/.themes
"$TEMP_DIR"/tokyonight-gtk/themes/install.sh -c dark -l --tweaks storm --dest ~/.themes

mkdir -p ~/.icons
icontheme=Tokyonight-Dark
if [ -d ~/.icons/"$icontheme" ]; then
  rm -rf ~/.icons/"$icontheme"
fi
cp -rf "$TEMP_DIR"/tokyonight-gtk/icons/"$icontheme" ~/.icons

echo -e "\nInstalling dependencies"
sudo pacman -S --noconfirm --needed \
  alacritty \
  alsa-utils \
  bat \
  btop \
  ca-certificates \
  cmus \
  ctags \
  curl \
  eza \
  fd \
  ffmpeg \
  firefox \
  fish \
  fzf \
  ghostscript \
  git-delta \
  gnome-keyring \
  grim \
  gtk-engine-murrine \
  imagemagick \
  imv \
  inter-font \
  jq \
  keyd \
  kitty \
  libappindicator-gtk3 \
  libnotify \
  libsecret \
  mako \
  man-db \
  moreutils \
  mpv \
  ncdu \
  nemo \
  nemo-fileroller \
  network-manager-applet \
  noto-fonts \
  openssh \
  otf-font-awesome \
  pavucontrol \
  playerctl \
  plocate \
  polkit-gnome \
  psmisc \
  pulseaudio \
  pulseaudio-alsa \
  python \
  rclone \
  slurp \
  starship \
  subdl \
  swappy \
  sway \
  swaybg \
  swayidle \
  swaylock \
  the_silver_searcher \
  ttf-joypixels \
  ttf-liberation \
  ttf-sourcecodepro-nerd \
  udiskie \
  unrar \
  unzip \
  waybar \
  wget \
  wl-clipboard \
  wlsunset \
  wofi \
  xdg-desktop-portal-wlr \
  yt-dlp \
  zathura \
  zathura-cb \
  zathura-pdf-poppler \
  zip \
  zoxide

systemctl --user mask gnome-keyring-daemon.service
systemctl --user mask gnome-keyring-daemon.socket

echo -e "\nInstalling dotfiles"
"$DOTFILES_ROOT"/install

echo -e "\nInstalling vim plugins"
if [ -e "$HOME"/.vim/autoload/plug.vim ]; then
  vim +PlugUpgrade +qa
else
  curl -fLo "$HOME"/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi
vim +PlugUpdate +PlugClean! +qa

echo -e "\nCreating user dirs"
mkdir -p ~/{doc,net,music,video} ~/pic/screenshots

echo -e "\nSetting up keyd"
sudo mkdir -p /etc/keyd/
sudo ln -sf "$DOTFILES_ROOT"/keyd/default.conf /etc/keyd/default.conf
sudo systemctl enable keyd

echo -e "\nSetting up asdf version manager"
if [ ! -d "$HOME/.asdf" ]; then
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf
fi
# Update to latest release.
(
  cd "$HOME/.asdf"
  git fetch origin --tags
  git -c advice.detachedHead=false checkout "$(git describe --abbrev=0 --tags)"
)

# shellcheck source=/dev/null
. "$HOME/.asdf/asdf.sh"

# Install dependencies for building Ruby on Arch Linux as per the link below.
# https://github.com/rbenv/ruby-build/wiki#suggested-build-environment
sudo pacman -S --noconfirm --needed base-devel rust libffi libyaml openssl zlib

if asdf plugin-list | grep -Fq ruby; then
  asdf plugin update ruby
else
  asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git
fi

echo -e "\nInstalling latest Ruby"
ruby_latest=$(asdf latest ruby)
if ! asdf list ruby | grep -Fq "$ruby_latest"; then
  asdf install ruby "$ruby_latest"
  asdf global ruby "$ruby_latest"
fi
gem update --system
bundle config --global jobs $(nproc)

echo '
Install complete. Exit the shell and log back in to start the desktop environment.

Have fun! :)
'
